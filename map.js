var infowindow = [];
var infoAgent = [];
var marker = [];
var agentsPin = [];

var occurrences = getOccurrences();
var agents = getAgents();

var map;

function initMap() {

    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: {lat: -19.868503, lng: -43.931447}
    });

    for (pos in occurrences){

        var icon = {
            url: occurrences[pos].type.image, // url
            scaledSize: new google.maps.Size(30, 30), // scaled size
        };        

        marker[pos] = new google.maps.Marker({
            position: {lat :  occurrences[pos].lat, lng :  occurrences[pos].lng},
            map: map,
            icon: icon,
            pos: pos
        });
        
        infowindow[pos] = new google.maps.InfoWindow({
            content: `
                <div class="infos">
                    <h4>${ occurrences[pos].type.name }</h4>
                    <p>${ occurrences[pos].status }</p>
                </div>
            `
        });

        google.maps.event.addListener(marker[pos], "click", function (event) {

            map.setCenter(event.latLng);
            map.setZoom(14);
        
            closeInfos();

            infowindow[this.pos].open(map, this);

            var encontrou = false;
            var mostrarTudo = false;
            var km = 3;
            while(encontrou == false){

                for (pos in agents){
                    if(typeof agentsPin[pos] != 'undefined'){
                        agentsPin[pos].setMap(null);          
                    }

                    let diference = distance(event.latLng.lat(), event.latLng.lng(), agents[pos].lat, agents[pos].lng, 'K');
                    if(mostrarTudo || diference < km){                                
                        encontrou = true;
                        
                        if(occurrences[this.pos].type.type_agents.indexOf(agents[pos].type.name) != -1){
                            var icon = {
                                url: agents[pos].type.image, // url
                                scaledSize: new google.maps.Size(25, 25), // scaled size
                            };
                        }else{
                            var icon = {
                                url: agents[pos].type.image, // url
                                scaledSize: new google.maps.Size(18, 18), // scaled size
                            };
                        }

                        infoAgent[pos] = new google.maps.InfoWindow({
                            content: `
                                <div class="infos">
                                    <h4>${ agents[pos].type.name }</h4>                                    
                                    <p> ( COD. ${ agents[pos].identify } ) </p>
                                </div>
                            `
                        }); 
                        
                        agentsPin[pos] = new google.maps.Marker({
                            position: {lat :  agents[pos].lat, lng :  agents[pos].lng},
                            map: map,
                            icon: icon,
                            pos: pos
                        });             
                        
                        google.maps.event.addListener(agentsPin[pos], "click", function (event) {
                            closeInfos();

                            infoAgent[this.pos].open(map, this);
                        });
                    }                                                          

                }

                if(encontrou == false && km <= 5)
                        km++;
                else if(mostrarTudo)
                    encontrou = true;
                else if(encontrou == false)
                    mostrarTudo = true;
                else
                    encontrou = true;
            }
        });


    }
}

function closeInfos(){
    for(key in infowindow){
        infowindow[key].close();
    }

    for(key in infoAgent){
        infoAgent[key].close();
    }
}

var checks = document.querySelectorAll(".check");
for(index in checks){
    checks[index].addEventListener("click", function(){
        for (pos in agentsPin){
            
            if((typeof agentsPin[pos] != 'undefined') &&
            (agents[agentsPin[pos].pos].type.name == this.name)){
                 agentsPin[pos].setMap(null);
            }
        }

        if(this.checked){
            for (pos in agents){
                if(agents[pos].type.name == this.name){
                    var icon = {
                            url: agents[pos].type.image, // url
                            scaledSize: new google.maps.Size(18, 18), // scaled size
                        };
                    agentsPin[pos] = new google.maps.Marker({
                        position: {lat :  agents[pos].lat, lng :  agents[pos].lng},
                        map: map,
                        icon: icon,
                        pos: pos
                    });
                }
            }
        }
    });
}